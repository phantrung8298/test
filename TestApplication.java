package com.test;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.zip.GZIPInputStream;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import okhttp3.*;


@SpringBootApplication
@RestController
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

	@GetMapping("/")
	String Test() {
		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder()
				.url("https://gitlab.com/phantrung8298/test/-/raw/main/test.json.gz")
				.build();

		try {
			Response response = client.newCall(request).execute();
			String stringJson = decompress(response);
			JsonArray jsonObject = JsonParser.parseString(stringJson).getAsJsonArray();
			Sponsored sponsored = new Gson().fromJson(jsonObject.get(0).toString(), Sponsored.class);
			return sponsored.getApplicableBudgetRuleId();

		} catch (Exception e) {
			System.out.println("decompress error:" + e.getMessage());
		}
		return "hello world";
	}

	String decompress(Response response) throws IOException {
		ResponseBody responseBody = response.body();
		Closeable closeThis = null;
		MediaType mediaType = MediaType.parse(response.header("Content-Type"));
		Charset charset = mediaType.charset();
		String result = "";
		try {
			
			InputStream inputStream = responseBody.byteStream();
			
			inputStream = new GZIPInputStream(inputStream);
			if (true) {
				
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader reader = new BufferedReader(inputStreamReader);

				String line;
				do {
					line = reader.readLine();
					if(line!=null){
						result = result.concat(line);
					}
				} while (line != null);
			}
		} catch(Exception e){
			throw e;
		} 
		return result;
	}

}
